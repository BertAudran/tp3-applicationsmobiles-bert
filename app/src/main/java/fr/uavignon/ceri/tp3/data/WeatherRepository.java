package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.net.URI;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.CityDao;
import fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResponse;
import fr.uavignon.ceri.tp3.data.webservice.WeatherResult;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.WeatherRoomDatabase.databaseWriteExecutor;

public class WeatherRepository {

    private static final String TAG = WeatherRepository.class.getSimpleName();

    private LiveData<List<City>> allCities;
    private MutableLiveData<City> selectedCity;

    private final OWMInterface api;

    public MutableLiveData<Boolean> isLoading=new MutableLiveData<Boolean>();
    public MutableLiveData<Throwable> webServiceThrowable=new MutableLiveData<>();

    private CityDao cityDao;

    volatile int nbAPILoads=0;

    public void loadWeatherAllCities(){
        List<City> cities=cityDao.getSynchrAllCities();
        nbAPILoads=cities.size();
        for (int i=0;i<cities.size();i++){
            loadWeatherCity(cities.get(i));
        }
        //nbAPILoads=allCities.getValue().size();
        /*
        for (int i=0;i<allCities.getValue().size();i++){
            loadWeatherCity(allCities.getValue().get(i));
        }

         */
    }


    public void loadWeatherCity(City city){
        isLoading.postValue(Boolean.TRUE);
        String key="9a9bffc5bdf4814400efc5bc4e19b3cf";
        String q=city.getName()+",,"+city.getCountryCode().toString();
        System.out.println(q);
        //api.getWeather(q,key);
        //final MutableLiveData<WeatherResponse> result = new MutableLiveData<>();

        Log.d("PRE REQUETE",q);

        api.getWeather(q, key).enqueue(
                new Callback<WeatherResponse>() {
                    @Override
                    public void onResponse(Call<WeatherResponse> call,
                                           Response<WeatherResponse> response) {
                        Log.d("API",response.toString());
                        Log.d("API", String.valueOf(response.body().main.temp));
                        WeatherResult.transferInfo(response.body(),city);
                        Log.d("API TEMP",city.getTemperature().toString());
                        updateCity(city);
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        //Throwable th=new Throwable("Erreur");
                        //webServiceThrowable.postValue(th);
                    }

                    @Override
                    public void onFailure(Call<WeatherResponse> call, Throwable t) {
                        Log.d("FAILURE API",t.getMessage());
                        if (nbAPILoads<=1){
                            isLoading.postValue(Boolean.FALSE);
                        }
                        else{
                            nbAPILoads=nbAPILoads-1;
                        }
                        webServiceThrowable.postValue(t);
                    }
                });

    }


    private static volatile WeatherRepository INSTANCE;

    public synchronized static WeatherRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository(application);
        }

        return INSTANCE;
    }

    public WeatherRepository(Application application) {
        WeatherRoomDatabase db = WeatherRoomDatabase.getDatabase(application);
        cityDao = db.cityDao();
        allCities = cityDao.getAllCities();
        selectedCity = new MutableLiveData<>();
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://api.openweathermap.org/data/2.5/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();

        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<City>> getAllCities() {
        return allCities;
    }

    public MutableLiveData<City> getSelectedCity() {
        return selectedCity;
    }




    public long insertCity(City newCity) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return cityDao.insert(newCity);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(newCity);
        return res;
    }

    public int updateCity(City city) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return cityDao.update(city);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedCity.setValue(city);
        return res;
    }

    public void deleteCity(long id) {
        databaseWriteExecutor.execute(() -> {
            cityDao.deleteCity(id);
        });
    }

    public void getCity(long id)  {
        Future<City> fcity = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return cityDao.getCityById(id);
        });
        try {
            selectedCity.setValue(fcity.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
