package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Collection;
import java.util.List;

public class WeatherResponse {
    public final Main main=null;
    public final Wind wind=null;
    public final Clouds clouds=null;
    public final List<Weather> weather=null;
    public final Integer dt=null;

    public static class Weather{
        public final String icon=null;
        public final String description=null;

    }


    public static class  Main{
        public final Float temp=null;
        public final Integer humidity=null;

    }
    public static class Wind{
        public final Float speed=null;
        public final Integer deg=null;
    }

    public static class Clouds{
        public final Integer all=null;
    }


}
