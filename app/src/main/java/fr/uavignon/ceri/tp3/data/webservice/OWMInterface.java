package fr.uavignon.ceri.tp3.data.webservice;

import fr.uavignon.ceri.tp3.data.City;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {
    @Headers({
            "Accept: json",
    })
    @GET("weather")
    public Call<WeatherResponse> getWeather(@Query("q") String query ,
                               @Query("appid") String APIkey);
}
