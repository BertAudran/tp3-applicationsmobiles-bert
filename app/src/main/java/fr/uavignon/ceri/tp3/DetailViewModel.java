package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.City;
import fr.uavignon.ceri.tp3.data.WeatherRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();
    private MutableLiveData<Boolean> isLoading;
    private WeatherRepository repository;
    private MutableLiveData<City> city;
    private  MutableLiveData<Throwable> webServiceThrowable;

    public DetailViewModel (Application application) {
        super(application);
        isLoading=new MutableLiveData<Boolean>();
        repository = WeatherRepository.get(application);
        isLoading=repository.isLoading;
        webServiceThrowable=repository.webServiceThrowable;
        city = new MutableLiveData<>();
    }

    public void setCity(long id) {
        repository.getCity(id);
        city = repository.getSelectedCity();
    }
    LiveData<City> getCity() {
        return city;
    }

    LiveData<Boolean> getIsLoading() {
        return isLoading;
    }

    LiveData<Throwable> getWebServiceThrowable(){return webServiceThrowable;}

    public void updateWeather(){
        repository.loadWeatherCity(city.getValue());
    }
}

